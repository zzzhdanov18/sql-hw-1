ALTER TABLE orders RENAME COLUMN _order_date TO order_date;
ALTER TABLE orders ALTER COLUMN order_date TYPE DATE using to_date(order_date, 'DD/MM/YYYY');

--2.3
SELECT * FROM orders
WHERE (EXTRACT(MONTH FROM order_date) BETWEEN 9 and 11) AND EXTRACT(YEAR FROM order_date) = 2022

--2.4
SELECT user_id,
	order_id,
    order_date,
    category,
    name,
    status,
    items,
	price,
	CASE
    	WHEN price = (SELECT max(price) FROM orders) and status = 'create_order' THEN 10
        ELSE 0
    END discount,
    CASE
    	WHEN price = (SELECT max(price) FROM orders) and status = 'create_order' then (price*0.9)::real
        ELSE price
    END new_price
FROM orders

--2.5
DELETE FROM orders 
WHERE status = 'cancel_order' OR items > 4

--2.6
SELECT
  substring(email FROM '@(.+?)\.') AS mail_index,
  count(*)
FROM users
WHERE gender = 'Male'
GROUP BY substring(email FROM '@(.+?)\.')
ORDER BY COUNT DESC
LIMIT 3


